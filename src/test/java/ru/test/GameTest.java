package ru.test;

import org.junit.jupiter.api.Test;
import ru.test.players.PaperShowingPlayer;
import ru.test.players.RandomPlayer;

import static org.junit.jupiter.api.Assertions.*;

public class GameTest {

    @Test
    public void shouldRunGame() {
        Player p1 = new PaperShowingPlayer();
        Player p2 = new RandomPlayer();
        Rules rules = new Rules();
        Game game = new Game(10, p1, p2, rules);
        GameResult result = game.run();
        assertNotNull(result);
    }
}
